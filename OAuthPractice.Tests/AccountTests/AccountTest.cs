﻿using System.Web.Http;
using FluentAssertions;
using OAuthPractice.ProtectedApi.Auth;
using OAuthPractice.ProtectedApi.Entities;
using Xunit;

namespace OAuthPractice.Tests.AccountTests
{
    public class AccountTest:ApiTestsBase
    {
        [Fact]
        public void Should_get_hello_message()
        {
            var result = ApiInvoker.InvokeGetRequest<string>("account/hello");

            result.Should().Be("Hello, World");
        }

        [Fact]
        public void Should_regester_an_account()
        {
            var user=new UserModel(){UserName = "richie2",Password = "Password2",ConfirmPassword = "Password2"};

            var result = ApiInvoker.InvokePostRequest<IHttpActionResult, UserModel>("account/register", user);


        }
    }
}